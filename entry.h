#ifndef sfps_entry_h
#define sfps_entry_h
#include "iostream"
#include "lexer.h"
#include "parser.h"
#include "symbol/char_symbol.h"
#include "symbol/token_string_val.h"
namespace salty_fish_parser {
    void execute() {
        while (true) {
            std::cout << ">: ";
            std::string line = std::string();
            std::getline(std::cin, line);
            lexer l = lexer();
            l.init(&line);
            while (true) {
                token t = token();
                l.get_next_token(&t);
                if (salty_fish_parser::eof == t.attribute_) {
                    break;
                }
                std::cout << t.attribute_ + " : " + t.val_.m_ << std::endl;
            }
            parser p;
            p.init(&line);
            p.parse();
            p.tree_.print();
        }
    }
}// namespace salty_fish_parser
#endif