#ifndef sfps_ast_node_h
#define sfps_ast_node_h
#include "iostream"
#include "mem.h"
#include "memory"
#include "symbol/ast_node_kind.h"
#include "symbol/char_symbol.h"
#include "symbol/token_string_val.h"
#include "vector"
namespace salty_fish_parser {
    class ast_node {
    public:
        std::vector<ast_node *> child_nodes_;
        std::string token_attribute_;
        ast_node_attribute ast_node_attribute_;
        bool is_primary_;
        mem value_;
        ast_node();
        ~ast_node();
        void init_from_lb();
        void init_from_rb();
        void init_from_num(const std::string &src);
        void init_from_unary_expr(const ast_node *src1, const ast_node *src2);
        void init_from_binary_expr(const ast_node *src1, const ast_node *src2, const ast_node *src3);
        void init_from_unary_operator(const std::string &src_val, const std::string &src_attribute);
        void init_from_binary_operator(const std::string &src_val, const std::string &src_attribute);
        void init_from_parentheses_expr(const ast_node *src1, const ast_node *src2, const ast_node *src3);
    };
    ast_node::ast_node() {
        this->is_primary_ = false;
        this->ast_node_attribute_.val_ = salty_fish_parser::ast_node_attribute::unset;
    }
    void ast_node::init_from_lb() {
        this->is_primary_ = true;
        this->ast_node_attribute_.val_ = ast_node_attribute::literal_expr;
        this->token_attribute_ = salty_fish_parser::lb;
        this->value_.m_ = string_symbol::lb;
    }
    void ast_node::init_from_rb() {
        this->is_primary_ = true;
        this->ast_node_attribute_.val_ = ast_node_attribute::literal_expr;
        this->token_attribute_ = salty_fish_parser::rb;
        this->value_.m_ = string_symbol::rb;
    }
    void ast_node::init_from_num(const std::string &src) {
        this->is_primary_ = true;
        this->value_.m_ = src;
        this->ast_node_attribute_.val_ = ast_node_attribute::literal_expr;
        this->token_attribute_ = salty_fish_parser::num;
    }
    void ast_node::init_from_unary_expr(const ast_node *src1, const ast_node *src2) {
        this->is_primary_ = false;
        this->ast_node_attribute_.val_ = ast_node_attribute::unary_expr;
        this->child_nodes_.push_back(const_cast<ast_node *>(src1));
        this->child_nodes_.push_back(const_cast<ast_node *>(src2));
    }
    void ast_node::init_from_binary_expr(const ast_node *src1, const ast_node *src2, const ast_node *src3) {
        this->is_primary_ = false;
        this->ast_node_attribute_.val_ = ast_node_attribute::binary_expr;
        this->child_nodes_.push_back(const_cast<ast_node *>(src1));
        this->child_nodes_.push_back(const_cast<ast_node *>(src2));
        this->child_nodes_.push_back(const_cast<ast_node *>(src3));
    }
    void ast_node::init_from_unary_operator(const std::string &src_val, const std::string &src_attribute) {
        this->ast_node_attribute_.val_ = ast_node_attribute::literal_expr;
        this->is_primary_ = true;
        this->token_attribute_ = src_attribute;
        this->value_.m_ = src_val;
    }
    void ast_node::init_from_binary_operator(const std::string &src_val, const std::string &src_attribute) {
        this->ast_node_attribute_.val_ = ast_node_attribute::literal_expr;
        this->is_primary_ = true;
        this->token_attribute_ = src_attribute;
        this->value_.m_ = src_val;
    }
    void ast_node::init_from_parentheses_expr(const ast_node *src1, const ast_node *src2, const ast_node *src3) {
        this->is_primary_ = false;
        this->ast_node_attribute_.val_ = ast_node_attribute::parenthesize_expr;
        this->child_nodes_.push_back(const_cast<ast_node *>(src1));
        this->child_nodes_.push_back(const_cast<ast_node *>(src2));
        this->child_nodes_.push_back(const_cast<ast_node *>(src3));
    }

    ast_node::~ast_node() = default;

}// namespace salty_fish_parser

#endif