#ifndef sfps_utils_h
#define sfps_utils_h
#include "climits"
#include "iostream"
namespace salty_fish_parser {
    class utils {
    public:
        static bool atoi(const std::string *src, int *dst);
    };

    bool utils::atoi(const std::string *src, int *dst) {
        if (*src == "-2147483648") {
            *dst = INT_MIN;
            return true;
        }
        if (*src == "2147483647") {
            *dst = INT_MAX;
            return true;
        }
        int sign = 1, base = 0, i = 0;
        while ((*src)[i] == ' ') {
            i++;
        }
        if ((*src)[i] == '-' || (*src)[i] == '+') {
            sign = 1 - 2 * ((*src)[i++] == '-');
        }
        while ((*src)[i] >= '0' && (*src)[i] <= '9') {
            if (base > INT_MAX / 10 || (base == INT_MAX / 10 && (*src)[i] - '0' > 7)) {
                if (sign == 1) {
                    *dst = INT_MAX;
                    return false;
                } else {
                    *dst = INT_MIN;
                    return false;
                }
            }
            base = 10 * base + ((*src)[i++] - '0');
        }
        *dst = base * sign;
        return true;
    }
}// namespace salty_fish_parser
#endif