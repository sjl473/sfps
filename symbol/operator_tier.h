#ifndef sfps_operator_tier_h
#define sfps_operator_tier_h
#include "iostream"
#include "token_attribute.h"
namespace salty_fish_parser {
  class operator_tier {
public:
    const static int tier_minus_one = -1;
    const static int tier_one = 1;
    const static int tier_two = 2;
    const static int tier_three = 3;
    const static int tier_four = 4;
    const static int tier_five = 5;
    const static int tier_six = 6;
    static int get_unary_operator_tier_from_attribute(const std::string &src);
    static int get_binary_operator_tier_from_attribute(const std::string &src);
  };
  int
  operator_tier::get_unary_operator_tier_from_attribute(const std::string &src) {
    if (src == token_attribute::plus || src == token_attribute::dash || src == token_attribute::bang) {
      return operator_tier::tier_six;
    }
    return operator_tier::tier_minus_one;
  }

  int
  operator_tier::get_binary_operator_tier_from_attribute(const std::string &src) {
    if (src == token_attribute::star || (src == token_attribute::slash)) {
      return operator_tier::tier_five;
    } else if (src == token_attribute::plus || src == token_attribute::dash) {
      return operator_tier::tier_four;
    } else if (src == token_attribute::equal_equal || src == token_attribute::bang_equal) {
      return operator_tier::tier_three;
    } else if ((src) == token_attribute::amp_amp) {
      return operator_tier::tier_two;
    } else if ((src) == token_attribute::pipe_pipe) {
      return operator_tier::tier_one;
    } else {
      return operator_tier::tier_minus_one;
    }
  }
}// namespace salty_fish_parser
#endif