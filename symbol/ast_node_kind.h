#ifndef sfps_ast_node_kind_h
#define sfps_ast_node_kind_h
namespace salty_fish_parser
{
  class ast_node_attribute
  {
  public:
    const static int unset = -1;
    const static int literal_expr = 0;
    const static int unary_expr = 1;
    const static int parenthesize_expr = 2;
    const static int binary_expr = 3;
    int val_;
    ast_node_attribute();
  };
  ast_node_attribute::ast_node_attribute() { this->val_ = -1; }
} // namespace salty_fish_parser
#endif