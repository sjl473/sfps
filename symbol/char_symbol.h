#ifndef sfps_char_symbol_h
#define sfps_char_symbol_h
namespace salty_fish_parser {
  class char_symbol {
public:
    const static char star;
    const static char plus;
    const static char lb;
    const static char rb;
    const static char dash;
    const static char single_space;
    const static char slash;
    const static char back_slash;
    const static char back_slash_zero;
    const static char left_scope;
    const static char right_scope;
    const static char one;
    const static char two;
    const static char three;
    const static char four;
    const static char five;
    const static char six;
    const static char seven;
    const static char eight;
    const static char nine;
    const static char zero;
    const static char back_slash_t;
    const static char back_slash_n;
    const static char back_slash_r;
    const static char amp;
    const static char equal;
    const static char pipe;
    const static char less;
    const static char great;
    char_symbol();
  };
  const char char_symbol::back_slash = '\\';
  const char char_symbol::slash = '/';
  const char char_symbol::dash = '-';
  const char char_symbol::lb = '(';
  const char char_symbol::rb = ')';
  const char char_symbol::single_space = ' ';
  const char char_symbol::back_slash_zero = '\0';
  const char char_symbol::star = '*';
  const char char_symbol::plus = '+';
  const char char_symbol::left_scope = '{';
  const char char_symbol::right_scope = '}';
  const char char_symbol::one = '1';
  const char char_symbol::two = '2';
  const char char_symbol::three = '3';
  const char char_symbol::four = '4';
  const char char_symbol::five = '5';
  const char char_symbol::six = '6';
  const char char_symbol::seven = '7';
  const char char_symbol::eight = '8';
  const char char_symbol::nine = '9';
  const char char_symbol::zero = '0';
  const char char_symbol::back_slash_t = '\t';
  const char char_symbol::back_slash_n = '\n';
  const char char_symbol::back_slash_r = '\r';
  const char char_symbol::amp = '&';
  const char char_symbol::equal = '=';
  const char char_symbol::pipe = '|';
  const char char_symbol::less = '<';
  const char char_symbol::great = '>';
  char_symbol::char_symbol() = default;
}// namespace salty_fish_parser
#endif