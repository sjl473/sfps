#ifndef sfps_string_symbol_h
#define sfps_string_symbol_h
#include "iostream"
namespace salty_fish_parser {
  class token_string_val {
public:
    const static std::string star;
    const static std::string plus;
    const static std::string lb;
    const static std::string rb;
    const static std::string dash;
    const static std::string single_space;
    const static std::string slash_;
    const static std::string back_slash;
    const static std::string back_slash_zero;
    const static std::string print_space;
    const static std::string bang;
    const static std::string amp;
    const static std::string bang_equal;
    const static std::string equal_equal;
    const static std::string amp_amp;
    const static std::string pipe_pipe;
    const static std::string left_scope;
    const static std::string right_scope;
    const static std::string global_word;
    const static std::string if_word;
    const static std::string while_word;
    const static std::string for_word;
    const static std::string else_word;
    const static std::string break_word;
    const static std::string continue_word;
    const static std::string to_word;
    const static std::string let_word;
    const static std::string do_word;
    const static std::string return_word;
    const static std::string false_word;
    const static std::string true_word;
    const static std::string func_word;
    token_string_val();
  };
  const std::string token_string_val::star = "*";
  const std::string token_string_val::plus = "+";
  const std::string token_string_val::lb = "(";
  const std::string token_string_val::rb = ")";
  const std::string token_string_val::dash = "-";
  const std::string token_string_val::single_space = " ";
  const std::string token_string_val::slash_ = "/";
  const std::string token_string_val::back_slash = "\\";
  const std::string token_string_val::back_slash_zero = "\0";
  const std::string token_string_val::print_space = "\' \'";
  const std::string token_string_val::bang = "!";
  const std::string token_string_val::amp = "&";
  const std::string token_string_val::bang_equal = "!=";
  const std::string token_string_val::equal_equal = "==";
  const std::string token_string_val::amp_amp = "&&";
  const std::string token_string_val::pipe_pipe = "||";
  const std::string token_string_val::left_scope = "{";
  const std::string token_string_val::right_scope = "}";
  const std::string token_string_val::global_word = "global";
  const std::string token_string_val::if_word = "if";
  const std::string token_string_val::while_word = "while";
  const std::string token_string_val::for_word = "for";
  const std::string token_string_val::else_word = "else";
  const std::string token_string_val::break_word = "break";
  const std::string token_string_val::continue_word = "continue";
  const std::string token_string_val::to_word = "to";
  const std::string token_string_val::let_word = "let";
  const std::string token_string_val::do_word = "do";
  const std::string token_string_val::return_word = "return";
  const std::string token_string_val::false_word = "false";
  const std::string token_string_val::true_word = "true";
  const std::string token_string_val::func_word = "func";
  token_string_val::token_string_val() = default;
}// namespace salty_fish_parser
#endif