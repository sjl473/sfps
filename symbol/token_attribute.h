#ifndef sfps_alias_h
#define sfps_alias_h
#include "iostream"
namespace salty_fish_parser {
  class token_attribute {
public:
    const static std::string num;
    const static std::string star;
    const static std::string dash;
    const static std::string plus;
    const static std::string lb;
    const static std::string rb;
    const static std::string eof;
    const static std::string back_slash;
    const static std::string slash;
    const static std::string space;
    const static std::string bad_token;
    const static std::string bang;
    const static std::string amp;
    const static std::string bang_equal;
    const static std::string equal_equal;
    const static std::string amp_amp;
    const static std::string pipe_pipe;
    const static std::string less;
    const static std::string less_eq;
    const static std::string greater;
    const static std::string greater_eq;
    const static std::string left_scope;
    const static std::string right_scope;
    const static std::string idf;
    const static std::string else_word;
    const static std::string if_word;
    const static std::string false_word;
    const static std::string global_word;
    const static std::string for_word;
    const static std::string break_word;
    const static std::string continue_word;
    const static std::string to_word;
    const static std::string let_word;
    const static std::string do_word;
    const static std::string return_word;
    const static std::string true_word;
    const static std::string while_word;
    const static std::string func_word;
  };


  const std::string token_attribute::num = "num";
  const std::string token_attribute::star = "star";
  const std::string token_attribute::dash = "dash";
  const std::string token_attribute::plus = "plus";
  const std::string token_attribute::lb = "left bracket";
  const std::string token_attribute::rb = "right bracket";
  const std::string token_attribute::eof = "end of the file";
  const std::string token_attribute::back_slash = "back slash";
  const std::string token_attribute::slash = "slash";
  const std::string token_attribute::space = "space";
  const std::string token_attribute::bad_token = "bad token";
  const std::string token_attribute::bang = "bang";
  const std::string token_attribute::amp = "amp";
  const std::string token_attribute::bang_equal = "bang equal";
  const std::string token_attribute::equal_equal = "equal equal";
  const std::string token_attribute::amp_amp = "amp amp";
  const std::string token_attribute::pipe_pipe = "pipe pipe";
  const std::string token_attribute::less = "less";
  const std::string token_attribute::less_eq = "less equal";
  const std::string token_attribute::greater = "greater";
  const std::string token_attribute::greater_eq = "greater equal";
  const std::string token_attribute::left_scope = "left scope";
  const std::string token_attribute::right_scope = "right scope";
  const std::string token_attribute::idf = "identifier token";
  const std::string token_attribute::else_word = "else keyword";
  const std::string token_attribute::if_word = "if keyword";
  const std::string token_attribute::false_word = "false word";
  const std::string token_attribute::global_word = "global word";
  const std::string token_attribute::for_word = "for word";
  const std::string token_attribute::break_word = "break word";
  const std::string token_attribute::continue_word = "continue word";
  const std::string token_attribute::to_word = "to word";
  const std::string token_attribute::let_word = "let word";
  const std::string token_attribute::do_word = "do word";
  const std::string token_attribute::return_word = "return word";
  const std::string token_attribute::true_word = "true word";
  const std::string token_attribute::while_word = "while word";
  const std::string token_attribute::func_word = "func word";
}// namespace salty_fish_parser
#endif
