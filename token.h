#ifndef sfps_token_h
#define sfps_token_h
#include "mem.h"
#include "symbol/token_attribute.h"
namespace salty_fish_parser {
  class token {
public:
    std::string attribute_;
    unsigned long idx_{};
    bool is_val_{};
    std::string content_;
    token() = default;
    void init(const std::string &src);
    void set_attribute(const std::string &src);
    void set_has_value();
    void set_content(const std::string &src);
  };
  void
  token::init(const std::string &src) {
    this->attribute_ = token_attribute::bad_token;
    this->idx_ = 0;
    this->is_val_ = false;
  }
  void
  token::set_attribute(const std::string &src) {
    this->attribute_ = src;
  }
  void
  token::set_has_value() {
    this->is_val_ = true;
  }
  void
  token::set_content(const std::string &src) {
    this->content_ = src;
  }

}// namespace salty_fish_parser
#endif
