#ifndef sfps_parser_h
#define sfps_parser_h
#include "ast.h"
#include "ast_node.h"
#include "lexer.h"
#include "symbol/operator_tier.h"
namespace salty_fish_parser {
  class parser {
public:
    std::vector<token> token_vec_;
    lexer l_{};
    unsigned long current_idx_{};
    parser();
    ast tree_{};
    void init(const std::string *src);
    void construct_trim_invalid(const std::string *src);
    void parse();
    void parse_binary_expr(int parent_tier);
    void set_root(const ast_node *src);
    void parse_primary();
    int get_current_unary_tier();
    int get_current_binary_tier();
    unsigned long get_idx() const;
    std::string get_current_token_attribute();
    void right_move();
    std::string get_current_token_value();
    token get_current_token();
    ast_node *get_root() const;
    unsigned long get_token_vec_size() const;
    bool is_out_of_range() const;
    bool is_prev_literal_token();
    void parse_num();
    void parse_parentheses_expr();
    void parse_unary_expr();
    void parse_scope_expr();
    void parse_variable_declaration();
  };

  void
  parser::construct_trim_invalid(const std::string *src) {
    std::list<token> token_list;
    token t;
    this->l_.get_next_token(&t);
    if (t.attribute_ != salty_fish_parser::eof || t.attribute_ != salty_fish_parser::bad_token || t.attribute_ != salty_fish_parser::space) {
      this->token_vec_.push_back(t);
    }
    while (true) {
      if (t.attribute_ == salty_fish_parser::eof) {
        break;
      }
      t = token();
      this->l_.get_next_token(&t);
      if (t.attribute_ != salty_fish_parser::eof || t.attribute_ != salty_fish_parser::bad_token || t.attribute_ != salty_fish_parser::space) {
        this->token_vec_.push_back(t);
      }
    }
    token_vec_.pop_back();
  }

  void
  parser::init(const std::string *src) {
    this->l_ = lexer();
    l_.init(src);
    this->construct_trim_invalid(src);
  }

  void
  parser::parse() {
    if (this->get_current_token_attribute() == token_string_val::left_scope) {

    } else if (this->get_current_token_attribute() == token_string_val::let) {

    } else if (this->get_current_token_attribute() == token_string_val::global) {

    } else if (this->get_current_token_attribute() == token_string_val::if_word) {

    } else if (this->get_current_token_attribute() == token_string_val::while_word) {

    } else if (this->get_current_token_attribute() == token_string_val::for_word) {
      
    }

    this->parse_binary_expr(operator_tier::tier_minus_one);
  }

  void
  parser::parse_binary_expr(int parent_tier) {
    if (this->get_current_unary_tier() == operator_tier::tier_minus_one || this->get_current_unary_tier() < parent_tier) {
      // judge current tier is literal node
      // second judgement for filtering <u1 u2 ... un e1>, for moving to next branch
      this->parse_primary();
      if (is_out_of_range()) {
        this->current_idx_--;
        return;
      }
    } else if ((this->current_idx_ != 0 && !this->is_prev_literal_token()) || this->current_idx_ == 0) {
      this->parse_unary_expr();
    }
    // above can be seen as <e1 b u* e2>, code below parse <e1 b e2> when cursor is at <b>
    while (true) {
      if (is_out_of_range()) {
        this->current_idx_--;
        return;
      }
      int tier_after_liter = this->get_current_binary_tier();
      if (tier_after_liter <= parent_tier) {
        return;
      }
      if (tier_after_liter == operator_tier::tier_minus_one) {
        std::cout << "error: invalid single literal expression after a fully parsed expression" << std::endl;
        std::abort();
      }
      auto *mid_operator_node = new ast_node;
      mid_operator_node->init_from_binary_operator(this->get_current_token_value(), this->get_current_token_attribute());
      this->right_move();
      if (this->is_out_of_range()) {
        std::cout << "error: no right val after binary operator" << std::endl;
        std::abort();
      }
      auto *global_left_expr_copy = this->get_root();
      this->parse_binary_expr(tier_after_liter);
      auto *new_binary_expr_root = new ast_node();
      new_binary_expr_root->init_from_binary_expr(global_left_expr_copy, mid_operator_node, this->get_root());
      this->set_root(new_binary_expr_root);
    }
  }

  void
  parser::parse_primary() {
    token t = this->get_current_token();
    if (lb == t.attribute_) {
      this->parse_parentheses_expr();
    } else if (num == t.attribute_) {
      this->parse_num();
    } else {
      std::cout << "error: unexpected token when parsing primary expression" << std::endl;
      std::abort();
    }
  }

  void
  parser::parse_num() {
    auto *node = new ast_node();
    node->init_from_num(this->get_current_token_value());
    this->right_move();
    this->set_root(node);
  }

  void
  parser::parse_parentheses_expr() {
    auto *lb_node = new ast_node();
    lb_node->init_from_lb();
    this->right_move();
    if (this->is_out_of_range()) {
      std::cout << "error: no mid val after lb operator" << std::endl;
      std::abort();
    }
    this->parse_binary_expr(operator_tier::tier_minus_one);
    if (this->get_current_token_attribute() != salty_fish_parser::rb) {
      std::cout << "error: unexpected token " + this->get_current_token_attribute() + ", " + "expected token: " + salty_fish_parser::rb << std::endl;
      std::abort();
    }
    auto *rb_node = new ast_node();
    rb_node->init_from_rb();
    this->right_move();
    auto *new_root = new ast_node();
    new_root->init_from_parentheses_expr(lb_node, this->get_root(), rb_node);
    this->set_root(new_root);
    set_root(new_root);
  }

  void
  parser::parse_unary_expr() {
    // case when <e1 b1 u1 u2 u3 un ... e2> when u1 has higher or equal tier than b1
    // ex: 1 * (2 + 3) + ++ 3, when parent tier is plus after ) and current tier is plus as well
    int unary_tier = this->get_current_unary_tier();
    auto *unary_node = new ast_node();
    unary_node->init_from_unary_operator(this->get_current_token_value(), this->get_current_token_attribute());
    this->right_move();
    this->parse_binary_expr(unary_tier);// back the recursion
    if (this->is_out_of_range()) {
      std::cout << "error: no standard lone unary after a binary / unary operator" << std::endl;
      std::abort();
    }
    // root will be updated after this recursion
    auto *new_root = new ast_node();
    new_root->init_from_unary_expr(unary_node, this->get_root());
    this->set_root(new_root);
  }


  //  private BlockStatementSyntax ParseBlockStatement()
  //  {
  //    var statements = ImmutableArray.CreateBuilder<StatementSyntax>();
  //    var openBraceToken = MatchToken(SyntaxKind.OpenBraceToken);
  //    while (Current.Kind != SyntaxKind.EndOfFileToken && Current.Kind != SyntaxKind.CloseBraceToken)
  //    {
  //      var startToken = Current;
  //      var statement = ParseStatement();
  //      statements.Add(statement);
  //      // If ParseStatement() did not consume any tokens,
  //      // we need to skip the current token and continue
  //      // in order to avoid an infinite loop.
  //      //
  //      // We don't need to report an error, because we'll
  //      // already tried to parse an expression statement
  //      // and reported one.
  //      if (Current == startToken)
  //        NextToken();
  //    }
  //    var closeBraceToken = MatchToken(SyntaxKind.CloseBraceToken);
  //    return new BlockStatementSyntax(openBraceToken, statements.ToImmutable(), closeBraceToken);
  //  }

  void
  parser::parse_scope_expr() {
    while (true) {
      if (this->get_current_token_attribute() == token_string_val::right_scope) {
        break;
      } else {
      }
    }
  }

  int
  parser::get_current_unary_tier() {
    return operator_tier::get_unary_operator_tier_from_attribute(this->token_vec_[this->current_idx_].attribute_);
  }
  bool
  parser::is_prev_literal_token() {
    if (this->get_current_binary_tier() == operator_tier::tier_minus_one && this->get_current_unary_tier() == operator_tier::tier_minus_one) {
      return true;
    }
    return false;
  }
  int
  parser::get_current_binary_tier() {
    return operator_tier::get_binary_operator_tier_from_attribute(this->token_vec_[this->current_idx_].attribute_);
  }

  std::string
  parser::get_current_token_attribute() {
    return this->token_vec_[this->current_idx_].attribute_;
  }

  void
  parser::right_move() {
    this->current_idx_++;
  }

  std::string
  parser::get_current_token_value() {
    return this->token_vec_[this->current_idx_].val_.m_;
  }

  ast_node *
  parser::get_root() const {
    return this->tree_.root_;
  }

  token
  parser::get_current_token() {
    return this->token_vec_[this->current_idx_];
  }

  void
  parser::set_root(const ast_node *src) {
    this->tree_.root_ = const_cast<ast_node *>(src);
  }

  unsigned long
  parser::get_idx() const {
    return this->current_idx_;
  }

  unsigned long
  parser::get_token_vec_size() const {
    return this->token_vec_.size();
  }

  bool
  parser::is_out_of_range() const {
    if (this->get_token_vec_size() <= this->get_idx()) {
      return true;
    }
    return false;
  }
  void
  parser::parse_variable_declaration() {
  }

  parser::parser() = default;
}// namespace salty_fish_parser
#endif
