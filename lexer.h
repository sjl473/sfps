#ifndef sfps_lexer_h
#define sfps_lexer_h
#include "list"
#include "symbol/char_symbol.h"
#include "symbol/token_string_val.h"
#include "token.h"
#include "utils.h"
namespace salty_fish_parser {
  class lexer {
public:
    std::string src_;
    unsigned long idx_{};
    lexer() = default;
    void init(const std::string &src);
    bool get_next_token(salty_fish_parser::token *dst);
    void right_move();
    char get_current_char();
    void eat_space();
    std::string get_num();
    std::string get_key_word();
  };
  void
  lexer::init(const std::string &src) {
    this->src_ = src;
  }
  bool
  lexer::get_next_token(salty_fish_parser::token *dst) {
    if (this->idx_ >= this->src_.length()) {
      return false;
    }
    if (this->get_current_char() == char_symbol::back_slash_zero) {// '\0'
      dst->set_attribute(token_attribute::eof);
      return true;
    } else if (this->get_current_char() == char_symbol::left_scope) {// '{'
      dst->set_attribute(token_attribute::left_scope);
      this->right_move();
      return true;
    } else if (this->get_current_char() == char_symbol::right_scope) {// '}'
      dst->set_attribute(token_attribute::right_scope);
      this->right_move();
      return true;
    } else if (this->get_current_char() == char_symbol::lb) {// '('
      dst->set_attribute(token_attribute::lb);
      this->right_move();
      return true;
    } else if (this->get_current_char() == char_symbol::rb) {// ')'
      dst->set_attribute(token_attribute::rb);
      this->right_move();
      return true;
    } else if (this->get_current_char() == char_symbol::plus) {// '+'
      dst->set_attribute(token_attribute::plus);
      this->right_move();
      return true;
    } else if (this->get_current_char() == char_symbol::dash) {// '-'
      dst->set_attribute(token_attribute::dash);
      this->right_move();
      return true;
    } else if (this->get_current_char() == char_symbol::slash) {// '/'
      dst->set_attribute(token_attribute::slash);
      this->right_move();
      return true;
    } else if (this->get_current_char() == char_symbol::star) {// '*'
      dst->set_attribute(token_attribute::star);
      this->right_move();
      return true;
    } else if (this->get_current_char() == char_symbol::single_space) {// ' '
      eat_space();
      dst->set_attribute(token_attribute::space);
      return true;
    } else if (this->get_current_char() == char_symbol::one || this->get_current_char() == char_symbol::two ||
               this->get_current_char() == char_symbol::three || this->get_current_char() == char_symbol::four ||
               this->get_current_char() == char_symbol::five || this->get_current_char() == char_symbol::six ||
               this->get_current_char() == char_symbol::seven || this->get_current_char() == char_symbol::eight ||
               this->get_current_char() == char_symbol::nine || this->get_current_char() == char_symbol::zero) {// '0' to '9'
      std::string num = this->get_num();
      dst->set_attribute(token_attribute::num);
      dst->set_content(num);
      return true;
    } else if (std::isalpha(this->get_current_char())) {
      std::string word = this->get_key_word();
      if (word == token_string_val::if_word) {// "if"
        dst->set_attribute(token_attribute::if_word);
        return true;
      } else if (word == token_string_val::else_word) {// "else"
        dst->set_attribute(token_attribute::else_word);
        return true;
      } else if (word == token_string_val::break_word) {// "break"
        dst->set_attribute(token_attribute::break_word);
        return true;
      } else if (word == token_string_val::true_word) { // "true"
        dst->set_attribute(token_attribute::true_word);
        return true;
      } else if (word == token_string_val::false_word) {// "false"
        dst->set_attribute(token_attribute::false_word);
        return true;
      } else if (word == token_string_val::do_word) {// ""
        dst->set_attribute(token_attribute::do_word);
        return true;
      } else if (word == token_string_val::while_word) {
        dst->set_attribute(token_attribute::while_word);
        return true;
      } else if (word == token_string_val::global_word) {
        dst->set_attribute(token_attribute::global_word);
        return true;
      } else if (word == token_string_val::continue_word) {
        dst->set_attribute(token_attribute::continue_word);
        return true;
      } else if (word == token_string_val::return_word) {
        dst->set_attribute(token_attribute::return_word);
        return true;
      } else if (word == token_string_val::let_word) {
        dst->set_attribute(token_attribute::let_word);
        return true;
      } else if (word == token_string_val::to_word) {
        dst->set_attribute(token_attribute::to_word);
        return true;
      } else if (word == token_string_val::func_word) {
        dst->set_attribute(token_attribute::func_word);
        return true;
      } else if (word == token_attribute::for_word) {
        dst->set_attribute(token_attribute::for_word);
        return true;
      } else {
        dst->set_attribute(token_attribute::idf);
        return true;
      }
    }
    return false;
  }
  char
  lexer::get_current_char() {
    return this->src_[this->idx_];
  }
  void
  lexer::right_move() {
    this->idx_++;
  }
  void
  lexer::eat_space() {
    while (true) {
      if (this->idx_ >= this->src_.length()) {
        return;
      }
      if (std::isspace(this->get_current_char()) != 0) {
        this->right_move();
      } else {
        break;
      }
    }
  }
  std::string
  lexer::get_num() {
    unsigned long temp = this->idx_;
    while (true) {
      if (this->idx_ >= this->src_.length() || std::isdigit(this->get_current_char()) == false) {
        break;
      } else {
        this->right_move();
      }
    }
    return this->src_.substr(temp, this->idx_ - temp);
  }
  std::string
  lexer::get_key_word() {
    unsigned long temp = this->idx_;
    while (true) {
      if (this->idx_ >= this->src_.length() || std::isalpha(this->get_current_char()) == false) {
        break;
      } else {
        this->right_move();
      }
    }
    return this->src_.substr(temp, this->idx_ - temp);
  }

}
#endif