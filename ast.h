#ifndef sfps_ast_h
#define sfps_ast_h
#include "ast_node.h"
#include "queue"
#include "stack"
#include "vector"
namespace salty_fish_parser {
  class ast {
public:
    ast_node *root_;
    std::queue<ast_node *> q_;
    ast_node *root_cpy_;
    void print();
    ast();
    ~ast();
  };
  ast::ast() {
    this->root_ = new ast_node();
    this->root_cpy_ = root_;
  }
  ast::~ast() {
  }
  void
  ast::print() {
    delete (root_cpy_);
    q_.push(this->root_);
    while (!this->q_.empty()) {
      ast_node *temp = this->q_.front();
      if (temp->is_primary_) {
        std::cout << temp->value_.m_ << std::endl;
      }
      this->q_.pop();
      if (!temp->is_primary_) {
        for (auto & child_node : temp->child_nodes_) {
          this->q_.push(child_node);
        }
      }
      delete (temp);
    }
  }
}// namespace salty_fish_parser
#endif