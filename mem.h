#ifndef sfps_mem_h
#define sfps_mem_h
#include <iostream>
namespace salty_fish_parser {
    class mem {
    public:
        std::string m_;
        mem();
        void push_back(const std::string *src);
    };
    void mem::push_back(const std::string *src) {
        unsigned long len = src->length();
        for (unsigned long i = 0; i < len; i++) {
            this->m_.push_back((*src)[i]);
        }
    }
    mem::mem() = default;
}// namespace salty_fish_parser
#endif